<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class NewController
{
    public function index()
    {
        $number = mt_rand(0, 100)/4;

        return new Response($number);
    }
}