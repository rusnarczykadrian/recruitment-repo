<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class TestController
{
    public function index()
    {
        $number = mt_rand(0, 100);

        return new Response($number);
    }
}